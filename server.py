# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from datetime import datetime, timedelta, date

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pyson import Eval, If, In, Get, Bool
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition
from trytond.backend.postgresql.database import LoggingCursor
import psycopg2
from psycopg2 import Error
import os
from .exceptions import DisableDatabaseWarning
from trytond.i18n import gettext
from . import queries
import paramiko
from sshtunnel import SSHTunnelForwarder
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report

STATES = {
    'readonly': Eval('state') != 'draft',
}


class Database(Workflow, ModelSQL, ModelView):
    'Database'
    __name__ = 'vps.databases'
    _rec_name = 'name'
    vps = fields.Many2One('vps.server', 'Server',
        required=True, )
    documents_electronic = fields.One2Many('vps.document_electronic', 'database', 'Documents Electronic')
    price_plan = fields.Many2One('vps.price_plan', 'Price Plan',)
    name = fields.Char('Name Database', required=True,)
    state = fields.Selection([
            ('enable', 'Enable'),
            ('disable', 'Disable'),
        ], 'State', readonly=True, required=True,)
    customer = fields.Many2One('party.party', 'Customer')
    invoices_expiration = fields.Function(fields.Integer('Invoices Expiration'),
            'get_invoices_expiration')
    electronic_invoice = fields.Boolean('Electronic Invoicing' )
    finish_date_certificate = fields.Date('Finish Date Certificate', states={
        'required': Bool(Eval('electronic_invoice')),
        'invisible': ~Bool(Eval('electronic_invoice'))
    })
    sended_email = fields.Boolean('Sended Email')

    @classmethod
    def __setup__(cls):
        super(Database, cls).__setup__()
        cls._order = [
            ('name', 'DESC'),
        ]
        cls._transitions |= set((
                ('disable', 'enable'),
                ('enable', 'disable'),
            ))
        cls._buttons.update({
            'disable': {
                'invisible': Eval('state') != 'enable',
                },
            'enable': {
                'invisible': Eval('state') != 'disable',
                },
        })

    @staticmethod
    def default_state():
        return 'enable'

    @classmethod
    @ModelView.button
    @Workflow.transition('disable')
    def disable(cls, records):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        for record in records:
            warning_key = 'disable_vps'
            if Warning.check(warning_key):
                raise DisableDatabaseWarning(warning_key, gettext('vps.msg_disable_warning'))
            record.allow_connections_database(False)
        pass

    @classmethod
    def search_rec_name(cls, name, clause):
        _, operator, value = clause
        if operator.startswith('!') or operator.startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [
            bool_op,
            ('name', operator, value),
            ('customer.name', operator, value),
            ('vps.name', operator, value),
        ]
        return domain

    @classmethod
    @ModelView.button
    @Workflow.transition('enable')
    def enable(cls, records):
        print(records, 'records')
        for record in records:
            print(record, 'este es mi record')
            record.allow_connections_database(True)
            print('termino')

    def allow_connections_database(self, allow):
        connection = None
        name = self.name
        try:
            # tunnel = self.get_tunnel_ssh()
            # tunnel.start()
            connection, tunnel = self.get_connection_database(self.name)
            cursor = connection.cursor()
            # if allow:
            #     # allow_conection = queries.allow_connection.format(name=name, option='true')
            #     allow_conection = queries.allow_connection.format(name=name)
            #     # connection_limit = queries.connection_limit.format(name=name, option=-1)
            #     self.write([self], {'state': 'enable'})
            # else:
            #     # allow_conection = queries.allow_connection.format(name=name, option='false')
            #     allow_conection = queries.allow_connection_two.format(name=name)
            #     # connection_limit = queries.connection_limit.format(name=name, option=0)
            #     self.write([self], {'state': 'disable'})
            # # cursor.execute(connection_limit)
            # print(allow_conection, 'allow_conection')
            allow_conection = queries.getnumberdocuments
            cursor.execute(allow_conection)
            print(cursor.fetchall())
            if cursor.rowcount == 0:
                print("La consulta se ejecutó correctamente")
            else:
                print("La consulta falló")
            connection.commit()
            print("process successfully in PostgreSQL ")
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error while executed process", error)
        finally:
            #closing database connection.
            if(connection):
                cursor.close()
                connection.close()
                tunnel.close()
                print("PostgreSQL connection is closed")

    def get_tunnel_ssh(self):
        mypkey = paramiko.RSAKey.from_private_key_file('/home/presik/.ssh/id_rsa')
        tunnel = SSHTunnelForwarder(
            (self.vps.ip_address, 7722),
            ssh_username=self.vps.user,
            ssh_pkey=mypkey,
            remote_bind_address=('localhost', 5432))
        return tunnel

    def get_connection_database(self, database):
        mypkey = paramiko.RSAKey.from_private_key_file('/home/presik/.ssh/id_rsa')
        tunnel = SSHTunnelForwarder(
            (self.vps.ip_address, 7722),
            ssh_username=self.vps.user,
            ssh_pkey=mypkey,
            remote_bind_address=('localhost', 5432))
        tunnel.start()
        connection = psycopg2.connect(
                user=self.vps.user,
                password=self.vps.password,
                host=tunnel.local_bind_host,
                port=tunnel.local_bind_port,
                database=database,
                cursor_factory=LoggingCursor,
            )
        return connection, tunnel

    def get_invoices_expiration(self, name):
        Invoice = Pool().get('account.invoice')
        Configuration = Pool().get('vps.configuration')
        config = Configuration(1)
        InvoicePaymentTermLineDelta = Pool().get('account.invoice.payment_term.line.delta')

        invoices = Invoice.search([
            ('party', '=', self.customer),
            ('state', 'in', ['validated', 'posted']),
        ])
        now = date.today()

        invoices_expired = 0
        if config and config.limit_days_expired:
            for invoice in invoices:
                invoice_payment_term_line_delta = InvoicePaymentTermLineDelta.search([
                ('line.payment', '=', invoice.payment_term),
                ])
                if invoice.invoice_date and invoice_payment_term_line_delta:
                    d_expired = int((now - invoice.invoice_date).days)
                    d_expired = int(d_expired - invoice_payment_term_line_delta[0].days)
                    if d_expired <= 0:
                        invoices_expired += 0
                    else:
                        if d_expired >= config.limit_days_expired:
                            invoices_expired += 1
                # setattr(invoice, 'days_expired', days_expired)
        return invoices_expired

    def notification_resolution():
        pool = Pool()
        Database = pool.get('vps.databases')
        Template = pool.get('email.template')
        start_date = date.today()
        config = pool.get('account.configuration')(1)
        date_1 = start_date+timedelta(days=30)
        date_2 = start_date+timedelta(days=15)
        date_3 = start_date+timedelta(days=8)
        date_4 = start_date+timedelta(days=2)
        databases = Database.search([
                    ('state', '=', 'enable'),
                    ('electronic_invoice', '=', 'True'),
                    ])
        if databases:
            for database in databases:
                if database.finish_date_certificate in [date_1 , date_2, date_3, date_4]:
                    email = database.customer.email
                    response = Template.send(config.template_email_confirm, database, email)
                    if response.status_code == 202:
                        Database.write([database], {'sended_email': True})


class Server(ModelSQL, ModelView):
    'Vps Server'
    __name__ = 'vps.server'
    _rec_name = 'name'
    supplier = fields.Many2One('vps.supplier', 'Supplier', ondelete='RESTRICT', required=True)
    active = fields.Boolean('Active')
    name = fields.Char('Name', required=True,)
    user = fields.Char('User', required=True, )
    ip_address = fields.Char('IP Address', required=True,)
    port = fields.Char('Port', required=True,)
    password = fields.Char('Password', required=True,)
    # route = fields.Char('Route',  states=STATES)
    databases = fields.One2Many('vps.databases', 'vps', 'Databases',)
    state = fields.Selection([
            ('stop', 'Stop'),
            ('execution', 'Execution'),
        ], 'State', required=True,)
    location = fields.Char('Location')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_active():
        return True

    @classmethod
    def disable_databases(cls):
        Database = Pool().get('vps.databases')
        databases = Database.search([
            ('state', '=', 'enable'),
            ('vps.active', '=', True),
        ])
        for db in databases:
            if db.price_plan and db.price_plan.kind != 'free' and db.invoices_expiration >= 1:
                db.allow_connections_database(False)

    def get_electronic_documents(self, period_start, period_end):
        response = []
        for database in self.databases:
            if database.state == 'enable':
                try:
                    print('voy por la conexión')
                    connection, tunnel = database.get_connection_database(database.name)
                    cursor = connection.cursor()
                    cursor.execute(queries.active_module.format(module='staff_payroll_electronic'))
                    is_active = cursor.fetchall()
                    if len(is_active) > 0:
                        query = queries.get_number_documents_one.format(period_start=period_start.start_date, period_end=period_end.end_date)
                    else:
                        query = queries.get_number_documents_two.format(period_start=period_start.start_date, period_end=period_end.end_date)
                    cursor.execute(query)
                    columns = list(cursor.description)
                    results = cursor.fetchall()
                    for row in results:
                        row_dict = {}
                        for i, column in enumerate(columns):
                            row_dict[column.name] = row[i]
                        print(row_dict)
                        response.append(row_dict)
                        # database.invoice_in = row_dict['invoice_in']
                        # database.invoice_out = row_dict['invoice_out']
                        # database.event = row_dict['event']
                        # database.payroll = row_dict['payroll']
                        # database.save()
                except KeyError:
                    print('ups tuve un error')
                    raise UserWarning
                finally:
                    if connection:
                        cursor.close()
                        connection.close()
                        tunnel.close()
        return response


class PricePlan(ModelSQL, ModelView):
    'Price Plan'
    __name__ = 'vps.price_plan'
    _rec_name = 'name'
    name = fields.Char('Name', required=True,)
    kind = fields.Selection([
            ('free', 'Free'),
            ('payment', 'Payment'),
        ], 'Kind', required=True,)


class DocumentElectronic(ModelSQL, ModelView):
    'Document Electronic'
    __name__ = 'vps.document_electronic'
    database = fields.Many2One('vps.databases', 'Database')
    period = fields.Many2One('account.period', 'Period')
    document_support = fields.Integer('Document Support', states={'readonly': True})
    invoice_in = fields.Integer('Invoice In', states={'readonly': True})
    invoice_out = fields.Integer('Invoice Out', states={'readonly': True})
    event = fields.Integer('Event', states={'readonly': True})
    payroll = fields.Integer('Payroll', states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(DocumentElectronic, cls).__setup__()
        cls._order = [
            ('database', 'DESC'),
        ]
        # cls._transitions |= set((
        #         ('disable', 'enable'),
        #         ('enable', 'disable'),
        #     ))
        cls._buttons.update({
            'sync': {},
        })

    @classmethod
    @ModelView.button
    def sync(cls, records):
        for record in records:
            try:
                database = record.database
                connection, tunnel = database.get_connection_database(database.name)
                cursor = connection.cursor()
                allow_conection = queries.get_number_documents.format(period_start=record.period.start_date, period_end=record.period.end_date)
                cursor.execute(allow_conection)
                columns = list(cursor.description)
                results = cursor.fetchall()
                print(results)
                for row in results:
                    row_dict = {}
                    for i, column in enumerate(columns):
                        row_dict[column.name] = row[i]
                    record.invoice_in = row_dict['invoice_in']
                    record.invoice_out = row_dict['invoice_out']
                    record.event = row_dict['event']
                    record.payroll = row_dict['payroll']
                    record.save()
            except KeyError:
                raise UserWarning
                pass
            finally:
                if connection:
                    cursor.close()
                    connection.close()
                    tunnel.close()
                    print("PostgreSQL connection is closed")


class DocumentElectronicReportStart(ModelView):
    'Document Electronic Report Start'
    __name__ = 'vps.document_electronic_report.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    vps = fields.Many2One('vps.server', 'Server', required=True)
    period_start = fields.Many2One('account.period', 'Period Start')
    period_end = fields.Many2One('account.period', 'Period End')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class DocumentElectronicWizard(Wizard):
    'Document Electronic Wizard'
    __name__ = 'vps.document_electronic_wizard'
    start = StateView('vps.document_electronic_report.start',
        'vps.document_electronic_report_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('vps.document_electronic_report')

    def do_print_(self, action):
        report_context = {
            'company': self.start.company.id,
            'vps': self.start.vps.id,
            'period_start': self.start.period_start.id,
            'period_end': self.start.period_end.id,
        }
        return action, report_context

    def transition_print_(self):
        return 'end'


class DocumentElectronicReport(Report):
    'Shop Daily categories'
    __name__ = 'vps.document_electronic_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Server = pool.get('vps.server')
        server = Server(data['vps'])
        Period = pool.get('account.period')
        period_start = Period(data['period_start'])
        period_end = Period(data['period_end'])

        response = server.get_electronic_documents(period_start, period_end)
        print(response)
        report_context['start_date'] = data['start_date']
        report_context['end_start'] = data['end_date']
        report_context['records'] = response
        return report_context
