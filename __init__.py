# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import server
from . import supplier


def register():
    Pool.register(
        configuration.Configuration,
        supplier.SupplierVps,
        server.Server,
        server.Database,
        server.PricePlan,
        server.DocumentElectronic,
        server.DocumentElectronicReportStart,
        module='vps', type_='model')
    Pool.register(
        server.DocumentElectronicWizard,
        module='vps', type_='wizard')
    Pool.register(
        server.DocumentElectronicReport,
        module='vps', type_='report')
