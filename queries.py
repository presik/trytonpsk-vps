# allow_connection = 'alter database "{name}" with allow_connections {option};'
allow_connection = "UPDATE pg_database SET datallowconn=true WHERE datname='{name}';"
allow_connection_two = "update pg_database set datallowconn=false where datname='{name}';"
# connection_limit = 'alter database "{name}" with allow_connections {limit};'
connection_limit = 'alter database "{name}" connection limit {limit};'
active_module = "SELECT name, state FROM ir_module WHERE name='{module}';"
# get_number_documents = """
#     SELECT z.period,
#     sum(z.records) FILTER (WHERE z.type_doc = 'invoice_in') as invoice_in,
#     sum(z.records) FILTER (WHERE z.type_doc = 'invoice_out') as invoice_out,
#     sum(z.records) FILTER (WHERE z.type_doc = 'payroll') as payroll,
#     sum(z.records) FILTER (WHERE z.type_doc = 'event') as "event"
#     FROM (
#     SELECT
#         date_trunc('month',a.date_effective) as period,
#         concat('payroll') as type_doc,
#         count(a.id) as records
#     FROM staff_payroll_electronic AS a
#     WHERE a.electronic_state = 'authorized'
#     GROUP by a.date_effective, type_doc
#     UNION ALL
#     SELECT
#         date_trunc('month',b.invoice_date) as period,
#         case
#         when b.type = 'out' then concat('invoice_out')
#         when b.type = 'in' then concat('invoice_in')
#         else null
#         end as type_doc,
#         count(b.id) as records
#     FROM account_invoice AS b
#     WHERE b.electronic_state = 'authorized'
#     GROUP BY b.invoice_date, type_doc
#     UNION ALL
#     SELECT date_trunc('month', d.date_acceptance)::TIMESTAMP WITH TIME ZONE as period,
#     concat('event') as type_doc,
#     count(d.id) as records
#     FROM account_invoice_event_radian as d
#     WHERE d.response_acceptance IS NOT NULL
#     GROUP BY date_trunc('month', d.date_acceptance), type_doc
#     UNION ALL
#     SELECT date_trunc('month', e.date_receive_good_service)::TIMESTAMP WITH TIME ZONE as period,
#     concat('event') as type_doc,
#     count(e.id) as records
#     FROM account_invoice_event_radian as e
#     WHERE e.response_receive_good_service IS NOT NULL
#     GROUP BY date_trunc('month', e.date_receive_good_service), type_doc) as z where z.period = {period};
# """
get_number_documents_one = """
SELECT z.period, 
    sum(z.records) FILTER (WHERE z.type_doc = 'invoice_in') as invoice_in,
    sum(z.records) FILTER (WHERE z.type_doc = 'invoice_out') as invoice_out,
    sum(z.records) FILTER (WHERE z.type_doc = 'payroll') as payroll,
    sum(z.records) FILTER (WHERE z.type_doc = 'event') as "event"
    FROM (
    SELECT 
        date_trunc('month',a.start) as period,
        concat('payroll') as type_doc,
        count(a.id) as records
    FROM staff_payroll_electronic AS a
    WHERE a.electronic_state = 'authorized'
    GROUP by date_trunc('month',a.start), type_doc
    UNION ALL
    SELECT 
        date_trunc('month',b.invoice_date) as period,
        case 
        when b.type = 'out' then 'invoice_out'
        else null
        end as type_doc,
        count(b.id) as records
    FROM account_invoice AS b
    WHERE b.electronic_state = 'authorized'
    GROUP BY date_trunc('month',b.invoice_date), type_doc
    UNION ALL
    SELECT 
        date_trunc('month',b.invoice_date) as period,
        case 
        when b.type = 'in' then concat('invoice_in')
        else null
        end as type_doc,
        count(b.id) as records
    FROM account_invoice AS b
    WHERE b.electronic_state = 'authorized'
    GROUP BY date_trunc('month',b.invoice_date), type_doc
    UNION ALL
    SELECT date_trunc('month', d.date_acceptance)::TIMESTAMP WITH TIME ZONE as period,
    concat('event') as type_doc, 
    count(d.id) as records
    FROM account_invoice_event_radian as d
    WHERE d.response_acceptance IS NOT NULL
    GROUP BY date_trunc('month', d.date_acceptance), type_doc
    UNION ALL
    SELECT date_trunc('month', d.date_tacit_acceptance)::TIMESTAMP WITH TIME ZONE as period,
    concat('event') as type_doc, 
    count(d.id) as records
    FROM account_invoice_event_radian as d
    WHERE d.response_tacit_acceptance IS NOT NULL
    GROUP BY date_trunc('month', d.date_tacit_acceptance), type_doc
    UNION ALL
    SELECT date_trunc('month', d.date_claim)::TIMESTAMP WITH TIME ZONE as period,
    concat('event') as type_doc, 
    count(d.id) as records
    FROM account_invoice_event_radian as d
    WHERE d.response_claim IS NOT NULL
    GROUP BY date_trunc('month', d.date_claim), type_doc
    UNION ALL
    SELECT date_trunc('month', d.date_acknowledgment)::TIMESTAMP WITH TIME ZONE as period,
    concat('event') as type_doc, 
    count(d.id) as records
    FROM account_invoice_event_radian as d
    WHERE d.response_acknowledgment IS NOT NULL
    GROUP BY date_trunc('month', d.date_acknowledgment), type_doc
    UNION ALL
    SELECT date_trunc('month', e.date_receive_good_service)::TIMESTAMP WITH TIME ZONE as period, 
    concat('event') as type_doc, 
    count(e.id) as records
    FROM account_invoice_event_radian as e
    WHERE e.response_receive_good_service IS NOT NULL
    GROUP BY date_trunc('month', e.date_receive_good_service), type_doc) as z where z.period BETWEEN '{period_start}' AND '{period_end}'
    GROUP BY z.period
    ORDER BY z.period;
"""

get_number_documents_two = """
SELECT z.period, 
    sum(z.records) FILTER (WHERE z.type_doc = 'invoice_in') as invoice_in,
    sum(z.records) FILTER (WHERE z.type_doc = 'invoice_out') as invoice_out,
    sum(z.records) FILTER (WHERE z.type_doc = 'event') as "event"
    FROM (
    SELECT 
        date_trunc('month',b.invoice_date) as period,
        case 
        when b.type = 'out' then 'invoice_out'
        else null
        end as type_doc,
        count(b.id) as records
    FROM account_invoice AS b
    WHERE b.electronic_state = 'authorized'
    GROUP BY date_trunc('month',b.invoice_date), type_doc
    UNION ALL
    SELECT 
        date_trunc('month',b.invoice_date) as period,
        case 
        when b.type = 'in' then concat('invoice_in')
        else null
        end as type_doc,
        count(b.id) as records
    FROM account_invoice AS b
    WHERE b.electronic_state = 'authorized'
    GROUP BY date_trunc('month',b.invoice_date), type_doc
    UNION ALL
    SELECT date_trunc('month', d.date_acceptance)::TIMESTAMP WITH TIME ZONE as period,
    concat('event') as type_doc, 
    count(d.id) as records
    FROM account_invoice_event_radian as d
    WHERE d.response_acceptance IS NOT NULL
    GROUP BY date_trunc('month', d.date_acceptance), type_doc
    UNION ALL
    SELECT date_trunc('month', d.date_tacit_acceptance)::TIMESTAMP WITH TIME ZONE as period,
    concat('event') as type_doc, 
    count(d.id) as records
    FROM account_invoice_event_radian as d
    WHERE d.response_tacit_acceptance IS NOT NULL
    GROUP BY date_trunc('month', d.date_tacit_acceptance), type_doc
    UNION ALL
    SELECT date_trunc('month', d.date_claim)::TIMESTAMP WITH TIME ZONE as period,
    concat('event') as type_doc, 
    count(d.id) as records
    FROM account_invoice_event_radian as d
    WHERE d.response_claim IS NOT NULL
    GROUP BY date_trunc('month', d.date_claim), type_doc
    UNION ALL
    SELECT date_trunc('month', d.date_acknowledgment)::TIMESTAMP WITH TIME ZONE as period,
    concat('event') as type_doc, 
    count(d.id) as records
    FROM account_invoice_event_radian as d
    WHERE d.response_acknowledgment IS NOT NULL
    GROUP BY date_trunc('month', d.date_acknowledgment), type_doc
    UNION ALL
    SELECT date_trunc('month', e.date_receive_good_service)::TIMESTAMP WITH TIME ZONE as period, 
    concat('event') as type_doc,
    count(e.id) as records
    FROM account_invoice_event_radian as e
    WHERE e.response_receive_good_service IS NOT NULL
    GROUP BY date_trunc('month', e.date_receive_good_service), type_doc) as z where z.period BETWEEN '{period_start}' AND '{period_end}'
    GROUP BY z.period
    ORDER BY z.period;"""